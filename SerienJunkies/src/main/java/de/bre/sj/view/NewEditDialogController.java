package de.bre.sj.view;

import de.bre.sj.model.watchlist.WLEntry;
import de.bre.sj.tvdb.TvDB;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NewEditDialogController {
	@FXML
	private TextField nameTxt;
	@FXML
	private TextField formatTxt;
	@FXML
	private TextField langTxt;
	@FXML
	private TextField seriesIdTxt;
	@FXML
	private TextField seasonTxt;
	@FXML
	private TextField episodeTxt;
	@FXML
	private TextField maxEpisodeTxt;
	@FXML
	private TextField linkTxt;

	private Stage dialogStage;
	private WLEntry wlEntry;
	private boolean okClicked;

	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	public void setWlEntry(WLEntry wlEntry) {
		this.wlEntry = wlEntry;

		nameTxt.setText(wlEntry.getName());
		formatTxt.setText(wlEntry.getFormat());
		langTxt.setText(wlEntry.getLang());
		seriesIdTxt.setText(wlEntry.getSeriesId());
		seasonTxt.setText(Integer.toString(wlEntry.getSeason()));
		episodeTxt.setText(Integer.toString(wlEntry.getEpisode()));
		maxEpisodeTxt.setText(Integer.toString(wlEntry.getMaxEpisode()));
		linkTxt.setText(wlEntry.getLink());
	}

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 * 
	 * @return
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	@FXML
	private void handleGetMaxEpisode() {
		String seriesId = seriesIdTxt.getText();
		if ("".equals(seriesId)) {
			seriesId = TvDB.getSeriesId(nameTxt.getText());
			seriesIdTxt.setText(seriesId);
		}
		maxEpisodeTxt.setText(TvDB.getMaxEpisode(seriesId, langTxt.getText(), seasonTxt.getText()));
	}

	/**
	 * Called when the user clicks ok.
	 */
	@FXML
	private void handleOk() {
		if (isInputValid()) {
			wlEntry.setName(nameTxt.getText());
			wlEntry.setFormat(formatTxt.getText());
			wlEntry.setLang(langTxt.getText());
			wlEntry.setSeriesId(seriesIdTxt.getText());
			wlEntry.setSeason(Integer.parseInt(seasonTxt.getText()));
			wlEntry.setEpisode(Integer.parseInt(episodeTxt.getText()));
			wlEntry.setMaxEpisode(Integer.parseInt(maxEpisodeTxt.getText()));
			wlEntry.setLink(linkTxt.getText());

			okClicked = true;
			dialogStage.close();
		}
	}

	/**
	 * Called when the user clicks cancel.
	 */
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Validates the user input in the text fields.
	 * 
	 * @return true if the input is valid
	 */
	private boolean isInputValid() {
		StringBuilder errorMessage = new StringBuilder(115);

		if (nameTxt.getText() == null || nameTxt.getText().length() == 0) {
			errorMessage.append("No valid name!\n");
		}
		if (langTxt.getText() == null || langTxt.getText().length() == 0) {
			errorMessage.append("No valid language!\n");
		}
		if (seasonTxt.getText() == null || seasonTxt.getText().length() == 0) {
			errorMessage.append("No valid season!\n");
		} else {
			// try to parse into an int.
			try {
				Integer.parseInt(seasonTxt.getText());
			} catch (NumberFormatException e) {
				errorMessage.append("No valid season (must be an integer)!\n");
			}
		}
		if (episodeTxt.getText() == null || episodeTxt.getText().length() == 0) {
			errorMessage.append("No valid episode!\n");
		} else {
			// try to parse into an int.
			try {
				Integer.parseInt(episodeTxt.getText());
			} catch (NumberFormatException e) {
				errorMessage.append("No valid episode (must be an integer)!\n");
			}
		}
		if (errorMessage.length() != 0) {
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct invalid fields");
			alert.setContentText(errorMessage.toString());

			alert.showAndWait();
			
			return false;
		}
		return true;
	}
}
