package de.bre.sj.view;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.sj.Main;
import de.bre.sj.model.rss.FeedMessage;
import de.bre.sj.model.rss.HosterLink;
import de.bre.sj.model.rss.RSSFeedParser;
import de.bre.sj.model.watchlist.WLEntry;
import de.bre.sj.model.watchlist.Watchlist;
import de.bre.sj.util.PreferencesStore;
import de.bre.sj.util.Utility;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.stage.StageStyle;

public class MainLayoutController {
	@FXML
	private TabPane tabPane;
	
	@FXML
	private TableView<FeedMessage> rssTable;
	@FXML
	private TableColumn<FeedMessage, String> dateRssColumn;
	@FXML
	private TableColumn<FeedMessage, String> langRssColumn;
	@FXML
	private TableColumn<FeedMessage, String> titelRssColumn;
	@FXML
	private TableColumn<FeedMessage, String> linkRssColumn;

	@FXML
	private TableView<HosterLink> linkTable;
	@FXML
	private TableColumn<HosterLink, String> hosterColumn;
	@FXML
	private TableColumn<HosterLink, String> linkColumn;

	@FXML
	private TableView<WLEntry> dbTable;
	@FXML
	private TableColumn<WLEntry, String> langDbColumn;
	@FXML
	private TableColumn<WLEntry, String> titleDbColumn;
	@FXML
	private TableColumn<WLEntry, Integer> seasonDbColumn;
	@FXML
	private TableColumn<WLEntry, Integer> episodeDbColumn;
	@FXML
	private TableColumn<WLEntry, Integer> maxEDbColumn;
	@FXML
	private TableColumn<WLEntry, String> formatDbColumn;
	@FXML
	private TableColumn<WLEntry, String> linkDbColumn;

	@FXML
	private Label newRSSTimelbl;
	@FXML
	private Label numberLbl;
	@FXML
	private Label lastCheckLbl;

	@FXML
	private Button loadFeedBtn;
	@FXML
	private Button copySerieLinkBtn;
	@FXML
	private Button copyDlLinkBtn;
	@FXML
	private Button editDBEntryBtn;
	@FXML
	private Button delDBEntryBtn;
	@FXML
	private Button incSeasonBtn;
	@FXML
	private Button incEpisodeBtn;
	@FXML
	private Button openLinkBtn;
	@FXML
	private Button episodeLinkBtn;

	@FXML
	private ProgressBar rssFeedProgress = new ProgressBar();

	@FXML
	private TextField titleTxt;
	@FXML
	private TextField dateTxt;
	@FXML
	private TextField langTxt;
	@FXML
	private TextField linkTxt;

	private static final Logger LOG = LoggerFactory.getLogger(Main.class);

	private static final File PFAD_XML = new File("./res/Watchlist.xml");
	private static final String FEED_URL = "http://serienjunkies.org/xml/feeds/episoden.xml";

	private Main mainApp;
	private PreferencesStore prefStore = new PreferencesStore();
	private Thread timeThread;

	private boolean changed;

	private ObservableList<WLEntry> wlEntrys = FXCollections.observableArrayList();
	private ObservableList<FeedMessage> feedMessages;
	private ObservableList<HosterLink> hosterLinks;

	/**
	 * The constructor. The constructor is called before the initialize() method.
	 */
	// public MainLayoutController() {}

	/**
	 * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		dateRssColumn.setCellValueFactory(cellData -> cellData.getValue().pubdateProperty());
		langRssColumn.setCellValueFactory(cellData -> cellData.getValue().langProperty());
		titelRssColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
		linkRssColumn.setCellValueFactory(cellData -> cellData.getValue().linkProperty());

		rssTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		rssTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showRssDetails(newValue));

		hosterColumn.setCellValueFactory(cellData -> cellData.getValue().hosterProperty());
		linkColumn.setCellValueFactory(cellData -> cellData.getValue().linkProperty());

		linkTable.setOnMouseClicked(mouseEvent -> {
			if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2) {
				Toolkit.getDefaultToolkit().getSystemClipboard()
						.setContents(new StringSelection(linkTable.getSelectionModel().getSelectedItem().getLink()), null);
			}
		});

		langDbColumn.setCellValueFactory(cellData -> cellData.getValue().langProperty());
		titleDbColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
		seasonDbColumn.setCellValueFactory(new PropertyValueFactory<WLEntry, Integer>("season"));
		episodeDbColumn.setCellValueFactory(new PropertyValueFactory<WLEntry, Integer>("episode"));
		maxEDbColumn.setCellValueFactory(new PropertyValueFactory<WLEntry, Integer>("maxEpisode"));
		formatDbColumn.setCellValueFactory(cellData -> cellData.getValue().formatProperty());
		linkDbColumn.setCellValueFactory(cellData -> cellData.getValue().linkProperty());

		dbTable.setOnMouseClicked(mouseEvent -> {
			if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
				if (mouseEvent.getClickCount() == 2) {
					handleEditEntry();
				}
			}
		});

		dbTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue == null) {
				editDBEntryBtn.setDisable(true);
				delDBEntryBtn.setDisable(true);
				incSeasonBtn.setDisable(true);
				incEpisodeBtn.setDisable(true);
				openLinkBtn.setDisable(true);
				episodeLinkBtn.setDisable(true);
			} else {
				editDBEntryBtn.setDisable(false);
				delDBEntryBtn.setDisable(false);
				incSeasonBtn.setDisable(false);
				incEpisodeBtn.setDisable(false);

				if ("".equals(dbTable.getSelectionModel().getSelectedItem().getLink())
						|| dbTable.getSelectionModel().getSelectedItem().getLink() == null) {
					openLinkBtn.setDisable(true);
					episodeLinkBtn.setDisable(true);
				} else {
					openLinkBtn.setDisable(false);
					episodeLinkBtn.setDisable(false);
				}
			}
		});
	}

	@FXML
	private void handleLoadRss() {
		loadFeed(false);
		loadFeedBtn.setDisable(true);
	}

	@FXML
	private void handleShowAll() {
		loadFeed(true);
		loadFeedBtn.setDisable(false);
	}

	@FXML
	private void handleCopyLink() {
		Toolkit.getDefaultToolkit().getSystemClipboard()
				.setContents(new StringSelection(rssTable.getSelectionModel().getSelectedItem().getLink()), null);
	}

	@FXML
	private void handleCopyDlLink() {
		StringBuilder urls = new StringBuilder();
		List<HosterLink> hosterLinks;
		for (FeedMessage feedMessage : rssTable.getSelectionModel().getSelectedItems()) {
			hosterLinks = parseWebSite(feedMessage.getTitle(), feedMessage.getLink());
			for (HosterLink hosterLink : hosterLinks) {
				// TODO fragen welcher hoster wenn es uploaded nicht gibt
				if (hosterLink.getHoster().equalsIgnoreCase("uploaded.to")) {
					urls.append(hosterLink.getLink()).append('\n');
				}
			}
		}
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(urls.toString()), null);
	}

	@FXML
	private void handleReset() {
		timeThread.interrupt();
		prefStore.setLastCheck(null);
		prefStore.setLastTitle(null);
		loadFeedBtn.setDisable(false);
		lastCheckLbl.setText("LastCheck: ");

		if (feedMessages != null) {
			feedMessages.removeAll(feedMessages);
		}
		if (hosterLinks != null) {
			hosterLinks.removeAll(hosterLinks);
		}
	}

	@FXML
	private void handleNewEntry() {
		WLEntry tempWlEntry = new WLEntry();
		if (mainApp.showNewEditDialog(tempWlEntry)) {
			wlEntrys.add(tempWlEntry);
			calCounter(false);
		}
	}

	@FXML
	private void handleEditEntry() {
		WLEntry selectedWlEntry = dbTable.getSelectionModel().getSelectedItem();
		if (selectedWlEntry != null) {
			if (mainApp.showNewEditDialog(selectedWlEntry)) {
				calCounter(false);
			}
		}
	}

	@FXML
	private void handleDelEntry() {
		Alert dialog = new Alert(AlertType.CONFIRMATION);
		dialog.initStyle(StageStyle.UTILITY);
		dialog.setTitle("Delet Series");
		dialog.setHeaderText("do you really want to delete this series?");

		Optional<ButtonType> result = dialog.showAndWait();
		if (result.get() == ButtonType.OK) {
			delDbEntry();
			calCounter(false);
		}
	}

	private void delDbEntry() {
		dbTable.getItems().remove(dbTable.getSelectionModel().getSelectedIndex());
	}

	@FXML
	private void handleIncSeason() {
		if (dbTable.getSelectionModel().getSelectedItem().incSeason()) {
			delDbEntry();
		}
		calCounter(false);
	}

	@FXML
	private void handleIncEpisode() {
		if (dbTable.getSelectionModel().getSelectedItem().incEpisode()) {
			delDbEntry();
		}
		calCounter(false);
	}

	@FXML
	private void handleOpenLink() {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null; // NOPMD by dbg on 27.08.14 15:18
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(new URI(dbTable.getSelectionModel().getSelectedItem().getLink()));
			} catch (IOException | URISyntaxException e) {
				String errMsg = "Could not open link.";
				if (LOG.isErrorEnabled()) {
					LOG.error(errMsg, e);
				}
				Utility.showExeptionDialog(errMsg, e);
			}
		}
	}

	@FXML
	private void handleEpisodeLink() {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null; // NOPMD by dbg on 27.08.14 15:18
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				String link = dbTable.getSelectionModel().getSelectedItem().getLink();
				int staffel = dbTable.getSelectionModel().getSelectedItem().getSeason();

				desktop.browse(new URI(link + "/alle-serien-staffeln.html#Staffel_" + staffel));
			} catch (IOException | URISyntaxException e) {
				String errMsg = "Could not open episode link.";
				if (LOG.isErrorEnabled()) {
					LOG.error(errMsg, e);
				}
				Utility.showExeptionDialog(errMsg, e);
			}
		}
	}

	@FXML
	private void handleSave() {
		try {
			JAXBContext context = JAXBContext.newInstance(Watchlist.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			Watchlist watchList = new Watchlist();
			watchList.setWlEntrys(wlEntrys);

			marshaller.marshal(watchList, PFAD_XML);

			changed = false;
		} catch (JAXBException e) {
			String errMsg = "Could not save data to file";
			if (LOG.isErrorEnabled()) {
				LOG.error(errMsg, e);
			}
			Utility.showExeptionDialog(errMsg + ":\n" + PFAD_XML.getPath(), e);
		}
	}

	@FXML
	private void handleSaveExit() {
		handleSave();
		exitProgramm();
	}

	@FXML
	private void handleExit() {
		if (changed) {
			ButtonType buttonTypeYes = new ButtonType("Ja");
			ButtonType buttonTypeNo = new ButtonType("Nein");
			ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

			Alert dialog = new Alert(AlertType.CONFIRMATION, null, buttonTypeYes, buttonTypeNo, buttonTypeCancel);
			dialog.initStyle(StageStyle.UTILITY);
			dialog.setTitle("Beenden ohne speichern?");
			dialog.setHeaderText("Es gab �nderungen. wollen sie diese speichern?");

			Optional<ButtonType> result = dialog.showAndWait();
			if (result.get() == buttonTypeYes) {
				handleSave();
				exitProgramm();
			} else if (result.get() == buttonTypeNo) {
				exitProgramm();
			}
		} else {
			exitProgramm();
		}
	}

	private void exitProgramm() {
		if (timeThread != null) {
			timeThread.interrupt();
		}
		Platform.exit();
	}

	private void updateRSSTimelbl(final String text) {
		Platform.runLater(() -> newRSSTimelbl.setText(text));
	}

	private void loadFeed(boolean all) {
		if (wlEntrys.isEmpty()) {
			// TODO meldung das keine eintr�ge vorhanden ausgeben
		} else {
			newRSSTimelbl.setVisible(false);
			rssFeedProgress.setVisible(true);
			rssFeedProgress.setProgress(-1.0f);

			timeThread = new Thread("checkTime") {
				@Override
				public void run() {
					long timeDiffNew;
					long timeDiffOld = checkTimeDiff();
					if (timeDiffOld > 0) {
						while (!checkDatum()) {
							if (isInterrupted()) {
								break;
							}
							timeDiffNew = checkTimeDiff();
							if (timeDiffNew < timeDiffOld) {
								updateRSSTimelbl("neuer feed in: " + timeDiffNew);
								timeDiffOld = timeDiffNew;
							}
							try {
								sleep(1000);
							} catch (InterruptedException e) {
								interrupt();
							}
						}
					}
					Platform.runLater(() -> loadFeedBtn.setDisable(false));
					if (isInterrupted()) {
						Platform.runLater(() -> newRSSTimelbl.setVisible(false));
					} else {
						updateRSSTimelbl("neuer feed!");
					}
				}
			};

			new Thread("loadFeed") {
				@Override
				public void run() {
					if (feedMessages != null) {
						feedMessages.removeAll(feedMessages);
					}

					// try {
					// TODO readfeed dosn't work
					feedMessages = FXCollections.observableList(
							new RSSFeedParser(FEED_URL).readFeed(new TreeSet<WLEntry>(wlEntrys), prefStore.getLastTitle(), all));
					rssTable.setItems(feedMessages);
					timeThread.start();

					Platform.runLater(() -> {
						lastCheckLbl.setText(prefStore.getLastCheck());
						rssFeedProgress.setProgress(0);
						rssFeedProgress.setVisible(false);
						newRSSTimelbl.setVisible(true);
					});
					// } catch (Exception e) {
					// if (LOG.isErrorEnabled()) {
					// LOG.error("rss feed parse error", e);
					// }
					// Dialogs.create().title("Error").masthead("rss feed parse error").showException(e);
					// }

				}
			}.start();
		}
	}

	private long checkTimeDiff() {
		String lastCheck = prefStore.getLastCheck();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMMM yyyy HH:mm:ss Z", new Locale("en"));
		if (!lastCheck.isEmpty()) {
			Date lastCheckDate = null;
			try {
				lastCheckDate = sdf.parse(lastCheck);
			} catch (ParseException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error("Could not check time diff", e);
				}

				Utility.showExeptionDialog(getClass().getName() + ".checkTimeDiff() parse error", e);
			}
			if (lastCheckDate != null) {
				return 15 * 60 - (new Date().getTime() - lastCheckDate.getTime()) / 1000;
			}
		}
		return 0;
	}

	private boolean checkDatum() {
		String lastCheck = prefStore.getLastCheck();
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMMM yyyy HH:mm:ss Z", new Locale("en"));
		if ("".equals(lastCheck)) {
			return true;
		} else {
			Date date1 = null;
			Date date2 = new Date();
			try {
				date1 = sdf.parse(lastCheck);
				date2.setTime(date1.getTime() + 15 * 60 * 1000);
			} catch (ParseException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error("could not check date", e);
				}
				Utility.showExeptionDialog(getClass().getName() + ".checkDatum() parse error", e);
			}
			return sdf.format(new Date()).compareTo(sdf.format(date2)) > 0;
		}
	}

	private void showRssDetails(FeedMessage feedMessage) {
		if (rssTable.getSelectionModel().getSelectedItems().size() == 1) {
			if (feedMessage != null) {
				copySerieLinkBtn.setDisable(false);
				copyDlLinkBtn.setDisable(true);
				titleTxt.setText(feedMessage.getTitle());
				dateTxt.setText(feedMessage.getPubdate());
				langTxt.setText(feedMessage.getLang());
				linkTxt.setText(feedMessage.getLink());

				hosterLinks = FXCollections.observableList(parseWebSite(feedMessage.getTitle(), feedMessage.getLink()));
				linkTable.setItems(hosterLinks);
			}

		} else {
			copySerieLinkBtn.setDisable(true);
			copyDlLinkBtn.setDisable(false);
			titleTxt.setText("");
			dateTxt.setText("");
			langTxt.setText("");
			linkTxt.setText("");
			if (hosterLinks != null) {
				hosterLinks.removeAll(hosterLinks);
			}
		}
	}

	private List<HosterLink> parseWebSite(String title, String url) {
		List<HosterLink> links = new ArrayList<>();
		BufferedReader input = null;

		try {
			URL aURL = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) aURL.openConnection();
			connection.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
			connection.setDoInput(true);
			String contentEncoding = connection.getContentEncoding();

			if (contentEncoding == null) {
				input = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
			} else if ("gzip".equalsIgnoreCase(contentEncoding)) {
				input = new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream()), "utf-8"));
			} else {
				input = new BufferedReader(new InputStreamReader(connection.getInputStream(), contentEncoding));
			}

			String line = input.readLine();
			boolean hasFound = false;
			while (line != null) {
				if (line.startsWith("<p><strong>")) {
					line = line.replaceAll("<p>|</?strong>|<br\\s*/?>", "");
					if (!hasFound && line.contains(title) || line.startsWith(title.substring(0, title.indexOf('E') + 4))
							&& line.endsWith(title.substring(title.indexOf('E') + 3))) {
						hasFound = true;
					} else if (hasFound) {
						hasFound = false;
						break;
					}
				} else if (hasFound && line.startsWith("<strong>Download:")) {
					line = line.substring(line.indexOf('\"') + 1).replaceAll("(<[Bb][Rr]\\s*/>)|(</[Pp]>)", "");

					String[] lineArray = line.split("\".+\\s");
					links.add(new HosterLink(lineArray[1], lineArray[0]));
				}
				line = input.readLine();
			}
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("could not parse website", e);
			}
			Utility.showExeptionDialog(getClass().getName() + ".parseWebSite() error", e);
		} finally {
			try {
				if (input != null) {
					input.close();
				}
			} catch (IOException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error("could not close bufferreader input", e);
				}
			}
		}
		return links;
	}

	public void setMainApp(Main main) {
		this.mainApp = main;

		if (PFAD_XML.exists()) {
			try {
				JAXBContext context = JAXBContext.newInstance(Watchlist.class);
				Unmarshaller unmarshaller = context.createUnmarshaller();

				Watchlist watchList = (Watchlist) unmarshaller.unmarshal(PFAD_XML);
				wlEntrys.addAll(watchList.getWlEntrys());
			} catch (JAXBException e) {
				String errMsg = "Could not load data from file";
				if (LOG.isErrorEnabled()) {
					LOG.error(errMsg, e);
				}
				Utility.showExeptionDialog(errMsg + ":\n" + PFAD_XML.getPath(), e);
			}

		}

		if (wlEntrys.isEmpty()){
			tabPane.getSelectionModel().select(1);
		}
		
		dbTable.setItems(wlEntrys);
		
		lastCheckLbl.setText(prefStore.getLastCheck());

		calCounter(true);

		loadFeed(false);
	}

	private void calCounter(boolean start) {
		if (!start) {
			changed = true;
			loadFeedBtn.setDisable(false);
		}

		int countRunningShows = 0;
		int countSameShows = 0;
		String lastName = null;

		for (WLEntry wlEntry : wlEntrys) {
			if (wlEntry.getName().equals(lastName)) {
				countSameShows++;
			}
			if (wlEntry.getEpisode() > 0) {
				countRunningShows++;
			}
			lastName = wlEntry.getName();
		}
		int allShows = wlEntrys.size() - countSameShows;
		numberLbl.setText("Anzahl: " + countRunningShows + "/" + countSameShows + "/" + allShows);
	}
}
