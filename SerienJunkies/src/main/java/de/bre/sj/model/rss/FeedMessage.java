package de.bre.sj.model.rss;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * Represents one RSS message
 */
public class FeedMessage {
	private StringProperty pubdate;
	private StringProperty lang;
	private StringProperty title;
	private StringProperty link;

	public FeedMessage() {
		this.pubdate = new SimpleStringProperty();
		this.lang = new SimpleStringProperty();
		this.title = new SimpleStringProperty();
		this.link = new SimpleStringProperty();
	}

	public String getPubdate() { // NOPMD by dbg on 27.08.14 15:20
		return pubdate.get();
	}

	public void setPubdate(String pubdate) { // NOPMD by dbg on 27.08.14 15:20
		this.pubdate.set(pubdate);
	}

	public StringProperty pubdateProperty() { // NOPMD by dbg on 27.08.14 15:20
		return pubdate;
	}

	public String getLang() { // NOPMD by dbg on 27.08.14 15:20
		return lang.get();
	}

	public void setLang(String lang) { // NOPMD by dbg on 27.08.14 15:20
		this.lang.set(lang);
	}

	public StringProperty langProperty() { // NOPMD by dbg on 27.08.14 15:20
		return lang;
	}

	public String getTitle() { // NOPMD by dbg on 27.08.14 15:20
		return title.get();
	}

	public void setTitle(String title) { // NOPMD by dbg on 27.08.14 15:20
		this.title.set(title);
	}

	public StringProperty titleProperty() { // NOPMD by dbg on 27.08.14 15:21
		return title;
	}

	public String getLink() { // NOPMD by dbg on 27.08.14 15:21
		return link.get();
	}

	public void setLink(String link) { // NOPMD by dbg on 27.08.14 15:21
		this.link.set(link);
	}

	public StringProperty linkProperty() { // NOPMD by dbg on 27.08.14 15:21
		return link;
	}

	@Override
	public String toString() {
		return "FeedMessage [pubDate=" + pubdate + ", lang=" + lang + ", title=" + title + ", link" + link + "]";
	}
}
