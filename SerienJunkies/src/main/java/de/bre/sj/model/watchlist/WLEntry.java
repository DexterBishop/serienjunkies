package de.bre.sj.model.watchlist;

import java.util.Optional;

import javax.xml.bind.annotation.XmlType;

import de.bre.sj.tvdb.TvDB;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;

@XmlType(propOrder = { "seriesId", "name", "lang", "season", "episode", "maxEpisode", "format", "link" })
public class WLEntry implements Comparable<WLEntry> {
	private StringProperty lang;
	private StringProperty name;
	private IntegerProperty season;
	private IntegerProperty episode;
	private IntegerProperty maxEpisode;
	private StringProperty format;
	private StringProperty link;
	private StringProperty seriesId;

	/**
	 * The default constructor of WLEntry. Set lang to de, name and seriesId to
	 * empty value, season to 1, episode and maxEpisode to 0, format to 720p and
	 * link to http://www.serienjunkies.de/.
	 */
	public WLEntry() {
		this("de", "", 1, 0, 0, "720p", "http://www.serienjunkies.de/", "");
	}

	/**
	 * The constructor of WLEntry
	 * 
	 * @param lang
	 *            language of the series
	 * @param name
	 *            name of the series
	 * @param season
	 *            actual season of the series
	 * @param episode
	 *            actual episode of the season
	 * @param maxEpisode
	 *            count of episodes of the season
	 * @param format
	 *            display format of the series
	 * @param link
	 *            a link to the series (like serienjunkies.de)
	 * @param seriesId
	 *            the series id on TVDB
	 */
	public WLEntry(String lang, String name, int season, int episode, int maxEpisode, String format, String link, String seriesId) {
		this.lang = new SimpleStringProperty(lang);
		this.name = new SimpleStringProperty(name);
		this.season = new SimpleIntegerProperty(season);
		this.episode = new SimpleIntegerProperty(episode);
		this.maxEpisode = new SimpleIntegerProperty(maxEpisode);
		this.format = new SimpleStringProperty(format);
		this.link = new SimpleStringProperty(link);
		this.seriesId = new SimpleStringProperty(seriesId);
	}

	public String getLang() { // NOPMD by dbg on 27.08.14 15:25
		return lang.get();
	}

	public void setLang(String lang) { // NOPMD by dbg on 27.08.14 15:25
		this.lang.set(lang);
	}

	public StringProperty langProperty() { // NOPMD by dbg on 27.08.14 15:25
		return lang;
	}

	public String getName() { // NOPMD by dbg on 27.08.14 15:25
		return name.get();
	}

	public void setName(String name) { // NOPMD by dbg on 27.08.14 15:25
		this.name.set(name);
	}

	public StringProperty nameProperty() { // NOPMD by dbg on 27.08.14 15:25
		return name;
	}

	public int getSeason() { // NOPMD by dbg on 27.08.14 15:25
		return season.get();
	}

	public void setSeason(int staffel) { // NOPMD by dbg on 27.08.14 15:25
		this.season.set(staffel);
	}

	public IntegerProperty seasonProperty() { // NOPMD by dbg on 27.08.14 15:25
		return season;
	}

	public int getEpisode() { // NOPMD by dbg on 27.08.14 15:25
		return episode.get();
	}

	public void setEpisode(int episode) { // NOPMD by dbg on 27.08.14 15:25
		this.episode.set(episode);
	}

	public IntegerProperty episodeProperty() { // NOPMD by dbg on 27.08.14 15:25
		return episode;
	}

	public int getMaxEpisode() { // NOPMD by dbg on 27.08.14 15:25
		return maxEpisode.get();
	}

	public void setMaxEpisode(int maxEpisode) { // NOPMD by dbg on 27.08.14 15:25
		this.maxEpisode.set(maxEpisode);
	}

	public IntegerProperty maxEpisodeProperty() { // NOPMD by dbg on 27.08.14 15:25
		return maxEpisode;
	}

	public String getFormat() { // NOPMD by dbg on 27.08.14 15:25
		return format.get();
	}

	public void setFormat(String format) { // NOPMD by dbg on 27.08.14 15:25
		this.format.set(format);
	}

	public StringProperty formatProperty() { // NOPMD by dbg on 27.08.14 15:25
		return format;
	}

	public String getLink() { // NOPMD by dbg on 27.08.14 15:25
		return link.get();
	}

	public void setLink(String link) { // NOPMD by dbg on 27.08.14 15:26
		this.link.set(link);
	}

	public StringProperty linkProperty() { // NOPMD by dbg on 27.08.14 15:26
		return link;
	}

	public String getSeriesId() { // NOPMD by dbg on 27.08.14 15:26
		return seriesId.get();
	}

	public void setSeriesId(String seriesId) { // NOPMD by dbg on 27.08.14 15:26
		this.seriesId.set(seriesId);
	}

	public StringProperty seriesIdProperty() { // NOPMD by dbg on 27.08.14 15:26
		return seriesId;
	}

	/**
	 * Increment the episode by 1.
	 * 
	 * @return true if series has ended and the user want to delete it, otherwise
	 *         false
	 */
	public boolean incEpisode() {
		int newEpisode = episode.get() + 1;
		if (newEpisode < maxEpisode.get() || episode.get() == 0) {
			episode.set(newEpisode);
			if (newEpisode == 1) {
				if (seriesId.get().equals("")) {
					seriesId.set(TvDB.getSeriesId(name.get()));
				}
				String newMaxEpisode = TvDB.getMaxEpisode(seriesId.get(), lang.get(), String.valueOf(season.get()));
				maxEpisode.set(Integer.parseInt(newMaxEpisode));
			}
		} else {
			int newMaxEpisode = Integer.parseInt(TvDB.getMaxEpisode(seriesId.get(), lang.get(), String.valueOf(season.get())));
			if (newMaxEpisode > maxEpisode.get()){
				maxEpisode.set(newMaxEpisode);
				episode.set(newEpisode);
			}else{
				return incSeason();
			}
		}
		return false;
	}

	/**
	 * Increment the season by 1.
	 * 
	 * @return true if series has ended and the user want to delete it, otherwise
	 *         false
	 */
	public boolean incSeason() {
		season.set(season.get() + 1);
		episode.set(0);
		maxEpisode.set(0);
		if (seriesId.get().equals("")) {
			seriesId.set(TvDB.getSeriesId(name.get()));
		}
		if (TvDB.getEnded(seriesId.get(), lang.get(), String.valueOf(season.get()))) {
			Alert dialog = new Alert(AlertType.CONFIRMATION);
			dialog.initStyle(StageStyle.UTILITY);
			dialog.setTitle("Series ended");
			dialog.setHeaderText("the series ended. delete from database?");
			
			Optional<ButtonType> result = dialog.showAndWait();
			if(result.get() == ButtonType.OK){
				return true;
			} else {
				name.set(name.get() + " -ended-");
			}
		}
		return false;
	}

	/**
	 * If the names are equal it compares the language.
	 */
	@Override
	public int compareTo(WLEntry entry) {
		if (this.name.get().compareTo(entry.name.get()) == 0) {
			return this.lang.get().compareTo(entry.lang.get());
		} else {
			return this.name.get().compareTo(entry.name.get());
		}
	}
}
