package de.bre.sj.model.rss;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HosterLink {
	private StringProperty hoster;
	private StringProperty link;

	public HosterLink() {
		this(null, null);
	}
	
	public HosterLink(String hoster, String link) {
		this.hoster = new SimpleStringProperty(hoster);
		this.link = new SimpleStringProperty(link);
	}

	public String getHoster() { // NOPMD by dbg on 27.08.14 15:21
		return hoster.get();
	}

	public void setHoster(String hoster) { // NOPMD by dbg on 27.08.14 15:21
		this.hoster.set(hoster);
	}

	public StringProperty hosterProperty() { // NOPMD by dbg on 27.08.14 15:21
		return hoster;
	}
	
	public String getLink() { // NOPMD by dbg on 27.08.14 15:21
		return link.get();
	}

	public void setLink(String link) { // NOPMD by dbg on 27.08.14 15:21
		this.link.set(link);
	}

	public StringProperty linkProperty() { // NOPMD by dbg on 27.08.14 15:21
		return link;
	}
}
