package de.bre.sj.model.watchlist;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Watchlist {
	private List<WLEntry> wlEntrys;

	/**
	 * Get the watchlist entry's.
	 * 
	 * @return a List with WLEntry
	 */
	@XmlElement(name = "Entry")
	public List<WLEntry> getWlEntrys() {
		return wlEntrys;
	}

	/**
	 * Set the watchlist entry's
	 * 
	 * @param wlEntrys
	 *            a List with WLEntry
	 */
	public void setWlEntrys(List<WLEntry> wlEntrys) {
		this.wlEntrys = wlEntrys;
	}
}
