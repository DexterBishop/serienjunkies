package de.bre.sj.model.rss;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.sj.model.watchlist.WLEntry;
import de.bre.sj.util.PreferencesStore;

public class RSSFeedParser {
	private static final Logger LOG = LoggerFactory.getLogger(RSSFeedParser.class);

	private static final String LAST_BUILD_DATE = "lastBuildDate";
	private static final String ITEM = "item";
	private static final String TITLE = "title";
	private static final String LINK = "link";
	private static final String PUB_DATE = "pubDate";

	private final URL url;
	private final PreferencesStore prefStore = new PreferencesStore();

	public RSSFeedParser(String feedUrl) {
		try {
			this.url = new URL(feedUrl);
		} catch (MalformedURLException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new RuntimeException(e);
		}
	}

	public List<FeedMessage> readFeed(Set<WLEntry> wlEntrys, String lastTitle, boolean all) {
		List<FeedMessage> feedEntries = new ArrayList<FeedMessage>();
		try {
			if (LOG.isInfoEnabled()) {
				LOG.info("RSSFeedParser START");
			}
			boolean isFeedHeader = true;
			boolean hasFound = false;
			boolean rssTitle = true;
			boolean firstTitle = false;
			// Set header values intial to the empty string
			String pubdate = "";
			String title = "";
			String link = "";
			String lastBuildDate = "";

			// First create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			InputStream inputStream = read();
			XMLEventReader eventReader = inputFactory.createXMLEventReader(inputStream);
			// Read the XML document
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					String localPart = event.asStartElement().getName().getLocalPart();
					switch (localPart) {
					case ITEM:
						if (isFeedHeader) {
							isFeedHeader = false;
							prefStore.setLastCheck(lastBuildDate);
							firstTitle = true;
						}
						event = eventReader.nextEvent();
						break;
					case TITLE:
						if (rssTitle) {
							rssTitle = false;
						} else {
							title = getCharacterData(event, eventReader);
							if (firstTitle) {
								prefStore.setLastTitle(title);
								firstTitle = false;
							}
							if (!all) {
								for (WLEntry wlEntry : wlEntrys) {
									if ("de".equals(wlEntry.getLang()) && title.startsWith("[DEUTSCH]")
											&& !(title.contains("Subbed") || title.contains("SUBBED"))
											|| "en".equals(wlEntry.getLang()) && title.startsWith("[ENGLISCH]")) {
										// sucht nach ., S, Zahl, E, Zahl
										Pattern pattern = Pattern.compile("([.]S\\d+E\\d+)+");
										Matcher matcher = pattern.matcher(title);
										String name = "";
										if (matcher.find()) {
											// gibt den string von 0 bis .S01E01
											// zur�ck und ersetzte [DEUTSCH]
											// durch nichts
											name = title.substring(0, matcher.start()).replaceAll("(\\[([A-Z-])+\\]\\s)+", "");
										}
										if (name.equalsIgnoreCase(wlEntry.getName().replace(" ", "."))
												&& title.contains(wlEntry.getFormat())) {
											// S, Zahl, E, Zahl
											pattern = Pattern.compile("(S\\d+E\\d+)+");
											matcher = pattern.matcher(title);
											while (matcher.find()) {
												if (matcher.group().compareTo(
														String.format("S%02dE%02d", wlEntry.getSeason(), wlEntry.getEpisode())) > 0) {
													hasFound = true;
													break;
												}
											}
											break;
										}
									}
								}
							}
						}
						break;
					case PUB_DATE:
						try {
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
							LocalDateTime dateTime = LocalDateTime.parse(getCharacterData(event, eventReader), formatter);
							formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
							pubdate = dateTime.format(formatter);
						} catch (DateTimeParseException e) {
							if (LOG.isWarnEnabled()) {
								LOG.warn("date coudn't formatted", e);
							}
							pubdate = "";
						}
						break;
					case LINK:
						link = getCharacterData(event, eventReader);
						break;
					case LAST_BUILD_DATE:
						lastBuildDate = getCharacterData(event, eventReader);
						// if (lastBuildDate.equals(db.getLastCheck())) {
						// System.out.println("(RSSFeedParser.readFeed) Kein neuer Feed.");
						// System.out.println("(RSSFeedParser.readFeed) ENDE");
						// return null;
						// }
						break;
					}
				} else if (event.isEndElement() && hasFound) {
					if (ITEM.equals(event.asEndElement().getName().getLocalPart())) {
						int anzahl = 0;
						boolean multi = false;
						String[] anzahlString = null;

						if (!all) {
							Pattern pattern = Pattern.compile("[S]\\d+[E]\\d+[-]\\d+");
							Matcher matcher = pattern.matcher(title);
							while (matcher.find()) {
								multi = true;
								anzahlString = matcher.group().split("-");
								anzahlString[0] = anzahlString[0].substring(anzahlString[0].indexOf('E') + 1);
								anzahl = Integer.parseInt(anzahlString[1]) - Integer.parseInt(anzahlString[0]);
							}
						}

						for (int i = 0; i <= anzahl; i++) {
							FeedMessage message = new FeedMessage();
							message.setPubdate(pubdate);
							int index = title.indexOf(']');
							message.setLang(title.substring(1, index));
							if (multi) {
								message.setTitle(title.substring(index + 2).replaceAll("[E]\\d+[-]\\d+",
										String.format("E%02d", Integer.parseInt(anzahlString[0]) + i)));
							} else {
								message.setTitle(title.substring(index + 2));
							}
							message.setLink(link);
							feedEntries.add(message);
						}
						event = eventReader.nextEvent();
						hasFound = false;
						continue;
					}

				}
			}
		} catch (XMLStreamException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new RuntimeException(e);
		}
		if (LOG.isInfoEnabled()) {
			LOG.info("RSSFeedParser ENDE");
		}
		return feedEntries;
	}

	private String getCharacterData(XMLEvent event, XMLEventReader eventReader) throws XMLStreamException {
		String result = "";
		event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result;
	}

	private InputStream read() {
		try {
			URLConnection conn = url.openConnection();
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
			conn.connect();

			return conn.getInputStream();
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error(e.getMessage());
			}
			throw new RuntimeException(e);
		}
	}
}
