package de.bre.sj;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.sj.model.watchlist.WLEntry;
import de.bre.sj.view.MainLayoutController;
import de.bre.sj.view.NewEditDialogController;

public class Main extends Application { // NOPMD by dbg on 27.08.14 15:26
	private static final Logger LOG = LoggerFactory.getLogger(Main.class);

	private transient Stage primaryStage;

	@Override
	public void start(Stage primaryStage) { // NOPMD by dbg on 27.08.14 15:27
		if (LOG.isInfoEnabled()) {
			LOG.info("Programm START");
		}
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Serienjunkies v2.4.0");
		this.primaryStage.getIcons().add(new Image("file:res/icon.jpg"));

		showTabLayout();
	}

	@Override
	public void stop() throws Exception {
		if (LOG.isInfoEnabled()) {
			LOG.info("Programm END");
		}
	}

	/**
	 * Shows the tab layout.
	 */
	private void showTabLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/MainLayout.fxml"));
			AnchorPane tabLayout = (AnchorPane) loader.load();

			// Give the controller access to the main app.

			MainLayoutController controller = loader.getController();
			controller.setMainApp(this);

			Scene scene = new Scene(tabLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("coudn't load fxml", e);
			}
		}
	}

	/**
	 * Shows the new/edit dialog.
	 * 
	 * @return is ok clicked
	 */
	public boolean showNewEditDialog(WLEntry wlEntry) {
		boolean okClicked = false;
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("view/NewEditDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Entry");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			dialogStage.setResizable(false);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the wlentry into the controller.
			NewEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setWlEntry(wlEntry);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			okClicked = controller.isOkClicked();
		} catch (IOException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("coudn't load fxml", e);
			}
		}
		return okClicked;
	}

	public static void main(String[] args) { // NOPMD by dbg on 27.08.14 15:26
		launch(args);
	}
}
