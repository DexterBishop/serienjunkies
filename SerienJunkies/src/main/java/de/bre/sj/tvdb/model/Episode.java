package de.bre.sj.tvdb.model;

import javax.xml.bind.annotation.XmlElement;

public class Episode {
	private String seasonNumber;
	private String episodeNumber;

	@XmlElement(name = "SeasonNumber")
	public String getSeasonNumber() { // NOPMD by dbg on 27.08.14 15:18
		return seasonNumber;
	}

	public void setSeasonNumber(String seasonNumber) { // NOPMD by dbg on 27.08.14 15:18
		this.seasonNumber = seasonNumber;
	}

	@XmlElement(name = "EpisodeNumber")
	public String getEpisodeNumber() { // NOPMD by dbg on 27.08.14 15:18
		return episodeNumber;
	}

	public void setEpisodeNumber(String episodeNumber) { // NOPMD by dbg on 27.08.14 15:18
		this.episodeNumber = episodeNumber;
	}
}
