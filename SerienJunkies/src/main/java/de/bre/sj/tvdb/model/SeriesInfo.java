package de.bre.sj.tvdb.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Data")
public class SeriesInfo {
	private Series series;
	private List<Episode> episodes;

	@XmlElement(name = "Series")
	public Series getSeries() { // NOPMD by dbg on 27.08.14 15:22
		return series;
	}

	public void setSeries(Series series) { // NOPMD by dbg on 27.08.14 15:22
		this.series = series;
	}
	
	@XmlElement(name = "Episode")
	public List<Episode> getEpisodes() { // NOPMD by dbg on 27.08.14 15:22
		return episodes;
	}

	public void setEpisodes(List<Episode> episodes) { // NOPMD by dbg on 27.08.14 15:22
		this.episodes = episodes;
	}
}
