package de.bre.sj.tvdb.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Data")
public class SeriesIds {
	private List<Series> series;

	@XmlElement(name = "Series")
	public List<Series> getSeries() { // NOPMD by dbg on 27.08.14 15:22
		return series;
	}

	public void setSeries(List<Series> series) { // NOPMD by dbg on 27.08.14 15:22
		this.series = series;
	}
}
