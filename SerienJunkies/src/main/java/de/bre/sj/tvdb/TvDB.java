package de.bre.sj.tvdb;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.bre.sj.tvdb.model.Episode;
import de.bre.sj.tvdb.model.Series;
import de.bre.sj.tvdb.model.SeriesIds;
import de.bre.sj.tvdb.model.SeriesInfo;
import de.bre.sj.util.Utility;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

public final class TvDB { // NOPMD by dbg on 27.08.14 15:24
	private static final Logger LOG = LoggerFactory.getLogger(TvDB.class);

	private static final String API_KEY = "2600A99864002F5C";

	private static final String SCHEME = "http";
	private static final String AUTHORITY = "thetvdb.com";
	private static final String PATH = "/api";
	private static final String GET_SERIES = "/GetSeries.php";
	private static final String GET_SERIES_PARAM = "seriesname=";

	private static final String SERIES_URL = SCHEME + "://" + AUTHORITY + PATH + "/" + API_KEY + "/series/";

	private TvDB() {}

	@SuppressWarnings("unchecked")
	private static <T> T getXmlObject(InputStream inputStream, Class<?> marschallClass) {
		try {
			JAXBContext context = JAXBContext.newInstance(marschallClass);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			return (T) unmarshaller.unmarshal(inputStream);
		} catch (JAXBException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("get xml object failed", e);
			}
			Utility.showExeptionDialog("TvDB.getXmlObject() error", e);
		}
		return null;
	}

	/**
	 * Get the series id for the given series name. If more than one matching series is found, a choice dialog is shown. If nothing found a
	 * input dialog is shown to type in the id.
	 * 
	 * @param name
	 *            the name of the series as a string
	 * @return the series id as a String
	 */
	public static String getSeriesId(String name) {
		String seriesId = null;
		try {
			SeriesIds seriesIds = getXmlObject(new URI(SCHEME, AUTHORITY, PATH + GET_SERIES, GET_SERIES_PARAM + name, null).toURL()
					.openStream(), SeriesIds.class);

			if (seriesIds.getSeries() == null) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("not found");
				dialog.setHeaderText("Series not Found. Please go to thetvdb.com.");
				dialog.setContentText("Please enter series Id or cancle:");
				
				Optional<String> id = dialog.showAndWait();
				if (id.isPresent()) {
					seriesId = id.get();
				}
			} else {
				if (seriesIds.getSeries().size() > 1 || !seriesIds.getSeries().get(0).getSeriesName().equals(name)) {
					Map<String, String> series = new HashMap<>(); // NOPMD by dbg on 27.08.14 15:25
					
					for (Series e : seriesIds.getSeries()) {
						series.put(e.getSeriesName(), e.getSeriesid());
					}

					ChoiceDialog<String> dialog = new ChoiceDialog<>(series.keySet().iterator().next(), series.keySet());	
					dialog.setTitle("Series");
					dialog.setHeaderText("found more than one match");
					dialog.setContentText("Please select series:");
					
					Optional<String> key = dialog.showAndWait();
					if (key.isPresent()) {
						seriesId = series.get(key.get());
					}
				} else if (seriesIds.getSeries().size() == 1) {
					seriesId = seriesIds.getSeries().get(0).getSeriesid();
				}
			}
		} catch (IOException | URISyntaxException e) {
			if (LOG.isErrorEnabled()) {
				LOG.error("get series id for " + name, e);
			}
			Utility.showExeptionDialog("TvDB.getSeriesId() error", e);
		}
		return seriesId;
	}

	/**
	 * 
	 * @param seriesId
	 *            the series id
	 * @param lang
	 *            the series language
	 * @return
	 */
	private static SeriesInfo getSeriesInfo(String seriesId, String lang) {
		if (seriesId != null && !"".equals(seriesId)) {
			try (ZipInputStream zipInputStream = new ZipInputStream(new URL(SERIES_URL + seriesId + "/all/" + lang + ".zip").openStream())) {
				ZipEntry zipEntry = zipInputStream.getNextEntry();
				while (!zipEntry.getName().equals(lang + ".xml")) {
					zipEntry = zipInputStream.getNextEntry();
				}

				int size = (int) zipEntry.getSize();
				byte[] bytes = new byte[size];

				int erg = 0;
				for (int c = zipInputStream.read(); c != -1; c = zipInputStream.read()) {
					bytes[erg] = (byte) c;
					erg++;
				}

				if (erg == size) {
					// FileOutputStream fileOutputStream = new FileOutputStream("./res/" + zipEntry.getName());
					// for (int i = 0; i < bytes.length; i++) {
					// fileOutputStream.write(bytes[i]);
					// }
					// fileOutputStream.close();

					return getXmlObject(new ByteArrayInputStream(bytes), SeriesInfo.class);
				} else {
					if (LOG.isErrorEnabled()) {
						LOG.error("bytes soll: " + zipEntry.getSize());
						LOG.error("bytes ist : " + erg);
					}
				}
			} catch (IOException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error("get series info for " + seriesId + "(" + lang + ") failed", e);
				}
				Utility.showExeptionDialog("TvDB.getSeriesInfo() error", e);
			}
		}
		return null;
	}

	/**
	 * Get the number of episodes of the season.
	 * 
	 * @param serisId
	 *            the series id
	 * @param lang
	 *            the series language
	 * @param season
	 *            actual season of the series
	 * @return the number of episodes of the season
	 */
	public static String getMaxEpisode(String serisId, String lang, String season) {
		SeriesInfo seriesInfo = getSeriesInfo(serisId, lang);
		String maxEpisode = "0";

		if (seriesInfo != null) {
			for (Episode e : seriesInfo.getEpisodes()) {
				if (Integer.parseInt(e.getSeasonNumber()) > Integer.parseInt(season)) {
					break;
				} else if (season.equals(e.getSeasonNumber())) {
					maxEpisode = e.getEpisodeNumber();
				}
			}
		}
		return maxEpisode;
	}

	/**
	 * Check if the series is over.
	 * 
	 * @param serisId
	 *            the series id
	 * @param lang
	 *            the series language
	 * @param season
	 *            actual season of the series
	 * @return True if the series is over, otherwise false.
	 */
	public static boolean getEnded(String serisId, String lang, String season) {
		SeriesInfo seriesInfo = getSeriesInfo(serisId, lang);

		if (seriesInfo != null) {
			if ("ended".equalsIgnoreCase(seriesInfo.getSeries().getStatus())) {
				for (Episode e : seriesInfo.getEpisodes()) {
					if (Integer.parseInt(e.getSeasonNumber()) >= Integer.parseInt(season)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
}
