package de.bre.sj.tvdb.model;

import javax.xml.bind.annotation.XmlElement;

public class Series {
	private String seriesId;
	private String seriesName;
	private String status;

	public String getSeriesid() { // NOPMD by dbg on 27.08.14 15:22
		return seriesId;
	}

	public void setSeriesid(String seriesid) { // NOPMD by dbg on 27.08.14 15:22
		this.seriesId = seriesid;
	}

	@XmlElement(name = "SeriesName")
	public String getSeriesName() { // NOPMD by dbg on 27.08.14 15:22
		return seriesName;
	}

	public void setSeriesName(String seriesName) { // NOPMD by dbg on 27.08.14 15:22
		this.seriesName = seriesName;
	}

	@XmlElement(name = "Status")
	public String getStatus() { // NOPMD by dbg on 27.08.14 15:22
		return status;
	}

	public void setStatus(String status) { // NOPMD by dbg on 27.08.14 15:22
		this.status = status;
	}
}
