package de.bre.sj.util;

import java.util.prefs.Preferences;

import de.bre.sj.Main;

public class PreferencesStore {
	// TODO observer?
	private static final String LAST_CHECK = "lastCheck";
	private static final String LAST_TITLE = "lastTitle";

	/**
	 * Load the date and time from the last RSS feed check from Preferences and
	 * give it as a String back.
	 * 
	 * @return returns the date and time from last RSS feed check as a String
	 */
	public String getLastCheck() {
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
		return prefs.get(LAST_CHECK, "");
//		String lastCheck = prefs.get(LAST_CHECK, "");
//		if (lastCheck == null) {
//			lastCheck = "";
//		}
//		return lastCheck;
	}

	/**
	 * Save the date and time from the last RSS feed check in to Preferences.
	 * 
	 * @param lastCheck
	 *            date and time as a String
	 */
	public void setLastCheck(String lastCheck) {
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
		if (lastCheck == null) {
			prefs.remove(LAST_CHECK);
		} else {
			prefs.put(LAST_CHECK, lastCheck);
		}
	}

	/**
	 * Load the last title from the last RSS feed check from Preferences and give
	 * it as a String back.
	 * 
	 * @return
	 */
	public String getLastTitle() {
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
		return prefs.get(LAST_TITLE, null);
//		String lastTitle = prefs.get(LAST_TITLE, null);
//		if (lastTitle == null) {
//			lastTitle = "";
//		}
//		return lastTitle;
	}

	/**
	 * Save the last title from the last RSS feed check in to Preferences.
	 * 
	 * @param lastTitle
	 *            the title as a String
	 */
	public void setLastTitle(String lastTitle) {
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
		if (lastTitle == null) {
			prefs.remove(LAST_TITLE);
		} else {
			prefs.put(LAST_TITLE, lastTitle);
		}
	}
}
